
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Detail Barang</h3>
                    <table class="table table-bordered">
                        <tr><td width='200'>Nama Barang</td><td><?php echo $nama_barang; ?></td></tr>
                        <tr><td width='200'>Satuan</td><td><?php echo $satuan; ?></td></tr>
                        <tr><td></td><td><a href="<?php echo site_url('barang') ?>" class="btn btn-default">Cancel</a></td></tr>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->