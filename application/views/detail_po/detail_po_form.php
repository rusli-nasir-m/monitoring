
<?php
 $options = [
    0 => 'Sample1',
    1 => 'Sample2',
 ];
?>
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box box-primary'>
                <div class='box-header'>                
                  <h3 class='box-title'>DETAIL_PO</h3>
                  </div>
                  <div class='box-body'>
                      
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Id Po</td>
            <td>
            <?php  $list_id_po = $options?>
            <?php echo form_dropdown('id_po', $list_id_po, $id_po,['class' => 'form-control select2']);?>
            <!--<input type="text" class="form-control form_date" name="id_po" id="id_po" placeholder="Id Po" value="<?php //echo $id_po; ?>" />-->
            <?php echo form_error('id_po') ?>
        </td>
	    <tr><td>Id Barang</td>
            <td>
            <?php  $list_id_barang = $options?>
            <?php echo form_dropdown('id_barang', $list_id_barang, $id_barang,['class' => 'form-control select2']);?>
            <!--<input type="text" class="form-control form_date" name="id_barang" id="id_barang" placeholder="Id Barang" value="<?php //echo $id_barang; ?>" />-->
            <?php echo form_error('id_barang') ?>
        </td>
	    <tr><td>Jumlah</td>
            <td>
            <input type="text" class="form-control" name="jumlah" id="jumlah" placeholder="Jumlah" value="<?php echo $jumlah; ?>" />
            <?php echo form_error('jumlah') ?>
        </td>
	    <tr><td>Satuan</td>
            <td>
            <input type="text" class="form-control" name="satuan" id="satuan" placeholder="Satuan" value="<?php echo $satuan; ?>" />
            <?php echo form_error('satuan') ?>
        </td>
	    <tr><td>Keluar</td>
            <td>
            <input type="text" class="form-control" name="keluar" id="keluar" placeholder="Keluar" value="<?php echo $keluar; ?>" />
            <?php echo form_error('keluar') ?>
        </td>
	    <tr><td>Sisa</td>
            <td>
            <input type="text" class="form-control" name="sisa" id="sisa" placeholder="Sisa" value="<?php echo $sisa; ?>" />
            <?php echo form_error('sisa') ?>
        </td>
	    <input type="hidden" name="id_detail_po" value="<?php echo $id_detail_po; ?>" />
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('detail_po') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table>
    </form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->