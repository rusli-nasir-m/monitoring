
<?php
 $options = [
    0 => 'Sample1',
    1 => 'Sample2',
 ];
?>
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box box-primary'>
                <div class='box-header'>                
                  <h3 class='box-title'>DETAIL_PP</h3>
                  </div>
                  <div class='box-body'>
                      
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Id Pp</td>
            <td>
            <?php  $list_id_pp = $options?>
            <?php echo form_dropdown('id_pp', $list_id_pp, $id_pp,['class' => 'form-control select2']);?>
            <!--<input type="text" class="form-control form_date" name="id_pp" id="id_pp" placeholder="Id Pp" value="<?php //echo $id_pp; ?>" />-->
            <?php echo form_error('id_pp') ?>
        </td>
	    <tr><td>Id Barang</td>
            <td>
            <?php  $list_id_barang = $options?>
            <?php echo form_dropdown('id_barang', $list_id_barang, $id_barang,['class' => 'form-control select2']);?>
            <!--<input type="text" class="form-control form_date" name="id_barang" id="id_barang" placeholder="Id Barang" value="<?php //echo $id_barang; ?>" />-->
            <?php echo form_error('id_barang') ?>
        </td>
	    <tr><td>Permintaan</td>
            <td>
            <input type="text" class="form-control" name="permintaan" id="permintaan" placeholder="Permintaan" value="<?php echo $permintaan; ?>" />
            <?php echo form_error('permintaan') ?>
        </td>
	    <tr><td>Diterima</td>
            <td>
            <input type="text" class="form-control" name="diterima" id="diterima" placeholder="Diterima" value="<?php echo $diterima; ?>" />
            <?php echo form_error('diterima') ?>
        </td>
	    <tr><td>Permintaan Satuan</td>
            <td>
            <input type="text" class="form-control" name="permintaan_satuan" id="permintaan_satuan" placeholder="Permintaan Satuan" value="<?php echo $permintaan_satuan; ?>" />
            <?php echo form_error('permintaan_satuan') ?>
        </td>
	    <tr><td>Diterima Satuan</td>
            <td>
            <input type="text" class="form-control" name="diterima_satuan" id="diterima_satuan" placeholder="Diterima Satuan" value="<?php echo $diterima_satuan; ?>" />
            <?php echo form_error('diterima_satuan') ?>
        </td>
	    <input type="hidden" name="id_detail_pp" value="<?php echo $id_detail_pp; ?>" />
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('detail_pp') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table>
    </form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->