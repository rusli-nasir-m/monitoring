<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <center><h2>Laporan Barang_keluar</h2></center>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Proyek</th>
		<th>Tanggal</th>
		<th>Keterangan</th>
<!--		<th>Nip</th>-->
		
            </tr><?php
            foreach ($barang_keluar_data as $barang_keluar)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo get_proyek($barang_keluar->id_proyek) ?></td>
		      <td><?php echo $barang_keluar->tanggal ?></td>
		      <td><?php echo $barang_keluar->keterangan ?></td>
<!--		      <td>--><?php //echo $barang_keluar->nip ?><!--</td>	-->
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>