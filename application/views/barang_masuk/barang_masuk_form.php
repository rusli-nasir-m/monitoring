
<?php
$options = [
	0 => 'Sample1',
	1 => 'Sample2',
];
?>
<!-- Main content -->
<section class='content'>
    <form action="<?php echo $action; ?>" method="post" id="frm_barang_masuk">
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>Barang Masuk</h3>
                </div>
                <div class='box-body'>
                    <table class='table table-bordered'>
                            <tr>
                                <td>No Po</td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="no_po" id="no_po" placeholder="No Po" value="<?php echo $no_po; ?>" />
<!--                                        <span class="input-group-btn">-->
<!--                                            <button id="cek_po" type="button" class="btn btn-primary" title="Cek PO"><i class="fa fa-search"></i></button>-->
<!--                                        </span>-->
                                    </div>
                                    <?php echo form_error('no_po') ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Subcount</td>
                                <td>
	                                <?php
	                                $list_id_subcount = $this->db->query("SELECT * FROM subcount")->result();
	                                $list_subcount[''] = '-- Pilih Subcount --';
	                                if($list_id_subcount){
		                                foreach ($list_id_subcount as $dia){
			                                $list_subcount[$dia->id_subcount] = $dia->nama_subcount;
		                                }
	                                }
	                                ?>
                                    <?= form_dropdown('id_subcount',$list_subcount,$id_subcount,[
                                        'class' => 'form-control select2'
                                    ])?>
									<?php echo form_error('id_subcount') ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Tanggal</td>
                                <td>
                                    <input type="text" class="form-control form_date" name="tanggal" id="tanggal" placeholder="Tanggal" value="<?php echo $tanggal; ?>" />
									<?php echo form_error('tanggal') ?>
                                </td>
                                <input type="hidden" name="id_masuk" value="<?php echo $id_masuk; ?>" />
                            </tr>
                        </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>Daftar Barang</h3>
                </div>
                <div class='box-body'>
                    <table class="table" width="100%" id="table_detail">
                        <thead>
                        <tr>
                            <th width="70%">Barang</th>
                            <th width="100px">Jumlah</th>
                            <th width="100px">Satuan</th>
                            <th width="200px">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($detail_bm){
	                        foreach ($detail_bm as $ppd){
		                        ?>
                                <tr>
                                    <td>
				                        <?php
				                        $list_id_barang = $this->db->query("SELECT * FROM barang")->result();
				                        $list_barang[''] = '-- Pilih Barang --';
				                        if($list_id_barang){
					                        foreach ($list_id_barang as $dia){
						                        $list_barang[$dia->id_barang] = $dia->nama_barang;
					                        }
				                        }
				                        ?>
				                        <?= form_dropdown('id_barang[]',$list_barang,$ppd->id_barang,[
					                        'class'=> 'form-control select2'
				                        ])?>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="jumlah[]" value="<?= $ppd->jumlah?>">
                                    </td>
                                    <td>
                                        <input type="hidden"class="form-control" name="id_detail_masuk[]" value="<?= $ppd->id_detail_masuk?>">
                                        <input type="text"class="form-control" name="satuan[]" value="<?= $ppd->satuan?>">
                                    </td>
                                    <td>
                                        <button type="button" class="tambah btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i></button>
                                        <button type="button" class="hapus btn btn-danger btn-sm btn-flat"><i class="fa fa-minus"></i></button>
                                    </td>
                                </tr>
		                        <?php
	                        }
                        }else{
	                        ?>
                            <tr>
                                <td>
			                        <?php
			                        $list_id_barang = $this->db->query("SELECT * FROM barang")->result();
			                        $list_barang[''] = '-- Pilih Barang --';
			                        if($list_id_barang){
				                        foreach ($list_id_barang as $dia){
					                        $list_barang[$dia->id_barang] = $dia->nama_barang;
				                        }
			                        }
			                        ?>
			                        <?= form_dropdown('id_barang[]',$list_barang,null,[
				                        'class'=> 'form-control select2'
			                        ])?>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="jumlah[]">
                                </td>
                                <td>
                                    <input type="hidden"class="form-control" name="id_detail_pp[]">
                                    <input type="text"class="form-control" name="satuan[]">
                                    <input type="hidden"class="form-control" name="diterima[]">
                                    <input type="hidden"class="form-control" name="diterima_satuan[]">
                                </td>
                                <td>
                                    <button type="button" class="tambah btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i></button>
                                    <button type="button" class="hapus btn btn-danger btn-sm btn-flat"><i class="fa fa-minus"></i></button>
                                </td>
                            </tr>
	                        <?php
                        }

                        ?>
                        </tbody>
                    </table>
                    <?php form_error("id_barang[]");?>
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                    <a href="<?php echo site_url('barang_masuk') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    </div>
    </form>
</section><!-- /.content -->
<script>
    $(document).ready(function () {
        $('.select2').select2({
            dropdownAutoWidth : true,
            width: '100%'
        });
        $('.form_date').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            todayBtn: true,
        });
        var tableDetail = $('#table_detail');
        $(tableDetail).on('click','.tambah',function () {
            var tr = $(this).parents('tr');
//            var tbody = $(this).parent('tbody');
            $.get('<?= site_url('barang_masuk/add_row_bm')?>',function (res) {
                tr.after(res);
                $('.select2').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                });
            })

        });
        $(tableDetail).on('click','.hapus',function () {
            var tbody = $(this).parents('tbody');
            var child = tbody.children();
            if(child.length <=1){
                alert("Row terakhir tidak dapat dihapus");
                return false;
            }
            var tr = $(this).parents('tr');
            tr.remove();
        });

//        $('#cek_po').onclick(function () {
//
//            $.post('',{
//                q: $('#no_po').val(),
//            },function(result){
//
//            });
//        });

//        $('#frm_barang_masuk').on('submit',function (e) {
//            e.preventDefault();
//            var id_barang = document.getElementsByName("id_barang[]");
//            for(i=0;i <= (id_barang.length-1);i++){
//                console.log(id_barang[i].value.length);
//            }
////            if((document.getElementsByName("id_barang[]").value).length === 0){
////                alert(" Maaf Barang belum dipilih!");
////                return false;
////            }
//        });
    })
</script>