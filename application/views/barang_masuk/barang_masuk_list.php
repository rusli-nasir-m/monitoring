<?php $hak_akses = $this->session->hak_akses;?>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>BARANG_MASUK LIST</h3>
                </div>
                <div class='box-body'>
					<?php echo ($hak_akses != 'project_manager')?  anchor('barang_masuk/create/','<i class="fa fa-plus"></i> Barang Masuk',array('class'=>'btn btn-danger btn-flat btn-sm')):null;?>
	                <?php echo anchor(site_url('barang_masuk/pdf'), '<i class="fa fa-print"></i>', ['class'=>"btn btn-social-icon btn-flat btn-sm", 'target' => '_blank']); ?>
                    <div class='row'>
                        <form action="<?php echo site_url('barang_masuk/index'); ?>" class="form-inline" method="get">
                            <div class='col-md-8'>
                                <div class ='form-group form-group-sm'>
                                    <label for='pwd'>Filter Berdasarkan: </label>
                                    <div class="input-group input-daterange">
                                        <input type="text" class="form-control form_date" name="dari" value="<?= $dari?>">
                                        <div class="input-group-addon">S/d</div>
                                        <input type="text" class="form-control to_date" name="sampai"  value="<?= $sampai?>">
                                    </div>
                                </div>
                            </div>

                            <div class='col-md-4'>
                                <div class ='form-group form-group-sm pull-right'>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                                        <span class="input-group-btn">
							  <button class="btn btn-sm btn-flat btn-primary" type="submit"><i class='fa fa-search'></i></button>
											<?php
											if ($q <> '')
											{
												?>
                                                <a href="<?php echo site_url('barang_masuk'); ?>" class="btn btn-sm btn-flat btn-default"><i class='fa fa-minus-square-o'></i>&nbsp;Reset</a>
												<?php
											}
											?>
							</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br>
                    <br>
                    <div class='table-responsive'>
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <tr>
                                <th>No</th>
                                <th>No Po</th>
                                <th>Subcount</th>
                                <th>Tanggal</th>
                                <th>Action</th>
                            </tr><?php
							foreach ($barang_masuk_data as $barang_masuk)
							{
								?>
                                <tr>
                                    <td width="80px"><?php echo ++$start ?></td>
                                    <td><?php echo $barang_masuk->no_po ?></td>
                                    <td><?php echo get_subcount($barang_masuk->id_subcount) ?></td>
                                    <td><?php echo date('d-m-Y',strtotime($barang_masuk->tanggal)) ?></td>
                                    <td style="text-align:center" width="140px">
										<?php
										echo anchor(site_url('barang_masuk/read/'.$barang_masuk->id_masuk),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-info btn-xs'));
										echo '  ';
										echo ($hak_akses != 'project_manager')? anchor(site_url('barang_masuk/update/'.$barang_masuk->id_masuk),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-primary btn-xs')):null;
										echo '  ';
										echo ($hak_akses != 'project_manager')? anchor(site_url('barang_masuk/delete/'.$barang_masuk->id_masuk),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-xs" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'):null;
										?>
                                    </td>
                                </tr>
								<?php
							}
							?>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
                        </div>
                        <div class="col-md-6 text-right">
							<?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

