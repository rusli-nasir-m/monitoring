
<?php
$options = [
	0 => 'Sample1',
	1 => 'Sample2',
];
?>
<!-- Main content -->
<section class='content'>
    <form action="<?php echo $action; ?>" method="post">
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>PO</h3>
                </div>
                <div class='box-body'>
                        <table class='table table-bordered'>
                            <tr><td>No Po</td>
                                <td>
                                    <input type="text" class="form-control" name="no_po" id="no_po" placeholder="No Po" value="<?php echo $no_po; ?>" />
									<?php echo form_error('no_po') ?>
                                </td>
                            <tr><td>Subcount</td>
                                <td>
	                                <?php
	                                $list_id_subcount = $this->db->query("SELECT * FROM subcount")->result();
	                                $list_subcount[''] = '-- Pilih Subcount --';
	                                if($list_id_subcount){
		                                foreach ($list_id_subcount as $dia){
			                                $list_subcount[$dia->id_subcount] = $dia->nama_subcount;
		                                }
	                                }
	                                ?>
	                                <?= form_dropdown('id_subcount',$list_subcount,$id_subcount,[
		                                'class' => 'form-control select2'
	                                ])?>
	                                <?php echo form_error('id_subcount') ?>
                                </td>
                            <tr><td>Proyek</td>
                                <td>
	                                <?php
	                                $list_id_proyek = $this->db->query("SELECT * FROM proyek")->result();
	                                $list_proyek[''] = '-- Pilih Proyek --';
	                                if($list_id_proyek){
		                                foreach ($list_id_proyek as $da){
			                                $list_proyek[$da->id_proyek] = $da->nama_proyek;
		                                }
	                                }

	                                ?>
	                                <?php echo form_dropdown('id_proyek', $list_proyek, $id_proyek,['class' => 'form-control select2']);?>
                                    <!--<input type="text" class="form-control form_date" name="id_proyek" id="id_proyek" placeholder="Id Proyek" value="<?php //echo $id_proyek; ?>" />-->
	                                <?php echo form_error('id_proyek') ?>
                                </td>
                                <input type="hidden" name="id_po" value="<?php echo $id_po; ?>" />
                            <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                                    <a href="<?php echo site_url('po') ?>" class="btn btn-default">Cancel</a></td></tr>

                        </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>Daftar Barang</h3>
                </div>
                <div class='box-body'>
                    <table class="table" width="100%" id="table_detail">
                        <thead>
                        <tr>
                            <th width="70%">Barang</th>
                            <th width="100px">Jumlah</th>
                            <th width="100px">Satuan</th>
                            <th width="200px">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
						<?php
						if($detail_pp){
							foreach ($detail_pp as $ppd){
								?>
                                <tr>
                                    <td>
										<?php
										$list_id_barang = $this->db->query("SELECT * FROM barang")->result();
										$list_barang[''] = '-- Pilih Barang --';
										if($list_id_barang){
											foreach ($list_id_barang as $dia){
												$list_barang[$dia->id_barang] = $dia->nama_barang;
											}
										}
										?>
										<?= form_dropdown('id_barang[]',$list_barang,$ppd->id_barang,[
											'class'=> 'form-control select2'
										])?>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="jumlah[]" value="<?= $ppd->permintaan?>">
                                    </td>
                                    <td>
                                        <input type="hidden"class="form-control" name="id_detail_po[]" value="<?= $ppd->id_detail_po?>">
                                        <input type="text"class="form-control" name="satuan[]" value="<?= $ppd->satuan?>">
                                        <input type="hidden"class="form-control" name="keluar[]" value="<?= $ppd->keluar?>">
                                        <input type="hidden"class="form-control" name="sisa[]" value="<?= $ppd->sisa?>">

                                    </td>
                                    <td>
                                        <button type="button" class="tambah btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i></button>
                                        <button type="button" class="hapus btn btn-danger btn-sm btn-flat"><i class="fa fa-minus"></i></button>
                                    </td>
                                </tr>
								<?php
							}
						}else{
							?>
                            <tr>
                                <td>
									<?php
									$list_id_barang = $this->db->query("SELECT * FROM barang")->result();
									$list_barang[''] = '-- Pilih Barang --';
									if($list_id_barang){
										foreach ($list_id_barang as $dia){
											$list_barang[$dia->id_barang] = $dia->nama_barang;
										}
									}
									?>
									<?= form_dropdown('id_barang[]',$list_barang,null,[
										'class'=> 'form-control select2'
									])?>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="jumlah[]">
                                </td>
                                <td>
                                    <input type="hidden"class="form-control" name="id_detail_po[]">
                                    <input type="text"class="form-control" name="satuan[]">
                                    <input type="hidden"class="form-control" name="keluar[]">
                                    <input type="hidden"class="form-control" name="sisa[]">
                                </td>
                                <td>
                                    <button type="button" class="tambah btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i></button>
                                    <button type="button" class="hapus btn btn-danger btn-sm btn-flat"><i class="fa fa-minus"></i></button>
                                </td>
                            </tr>
							<?php
						}
						?>

                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                    <a href="<?php echo site_url('pp') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    </div>
    </form>
</section><!-- /.content -->
<script>
    $(document).ready(function () {
        var tableDetail = $('#table_detail');
        $(tableDetail).on('click','.tambah',function () {
            var tr = $(this).parents('tr');
//            var tbody = $(this).parent('tbody');
            $.get('<?= site_url('po/add_row_po')?>',function (res) {
                tr.after(res);
            })

        });
        $(tableDetail).on('click','.hapus',function () {
            var tbody = $(this).parents('tbody');
            var child = tbody.children();
            if(child.length <=1){
                alert("Row terakhir tidak dapat dihapus");
                return false;
            }
            var tr = $(this).parents('tr');
            tr.remove();
        })
    })
</script>