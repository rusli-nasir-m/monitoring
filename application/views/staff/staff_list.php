
<section class='content'>
  <div class='row'>
    <div class='col-xs-12'>
      <div class='box box-primary'>
        <div class='box-header'>
          <h3 class='box-title'>STAFF LIST</h3>
        </div>
        <div class='box-body'>
            <div class='row'>
            <form action="<?php echo site_url('staff/index'); ?>" class="form-inline" method="get">
				<div class='col-md-8'>
					<?php echo anchor('staff/create/','Create',array('class'=>'btn btn-danger btn-flat btn-sm'));?>
					<?php echo anchor(site_url('staff/pdf'), '<i class="fa fa-file-pdf-o"></i>', 'class="btn btn-social-icon btn-flat btn-sm"'); ?>
				</div>
				
				<div class='col-md-4'>
					<div class ='form-group form-group-sm pull-right'>												
						<div class="input-group">						
							<input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
							<span class="input-group-btn">								
							  <button class="btn btn-sm btn-flat btn-primary" type="submit"><i class='fa fa-search'></i></button>
							  <?php 
									if ($q <> '')
									{
										?>
										<a href="<?php echo site_url('staff'); ?>" class="btn btn-sm btn-flat btn-default"><i class='fa fa-minus-square-o'></i>&nbsp;Reset</a>
										<?php
									}
								?>
							</span>
						</div>						
					</div>
				</div>				
            </form>            			
		</div>
		<br>
		<br>
        <div class='table-responsive'>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Nama Staff</th>
		<th>Bagian</th>
		<th>Foto</th>
		<th>Alamat</th>
		<th>Hak Akses</th>
		<th>Action</th>
            </tr><?php
            foreach ($staff_data as $staff)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $staff->nama_staff ?></td>
			<td><?php echo $staff->bagian ?></td>
			<td><img src="<?= get_image_staff($staff->foto,250) ?>" alt="" class="img-thumbnail" width="100"></td>
			<td><?php echo $staff->alamat ?></td>
			<td><?php echo $staff->hak_akses ?></td>
		    <td style="text-align:center" width="140px">
			<?php 
			echo anchor(site_url('staff/read/'.$staff->nip),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-info btn-xs'));
			echo '  '; 
			echo anchor(site_url('staff/update/'.$staff->nip),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-primary btn-xs'));
			echo '  '; 
			echo anchor(site_url('staff/delete/'.$staff->nip),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-xs" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
		</tr>
                <?php
            }
            ?>
        </table>
		</div>
		<div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
			</div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>    

