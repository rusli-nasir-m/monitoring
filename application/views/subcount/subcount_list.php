
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>SUBCOUNT LIST</h3>
                </div>
                <div class='box-body'>
					<?php echo anchor('subcount/create/','Create',array('class'=>'btn btn-danger btn-flat btn-sm'));?>
	                <?php echo anchor(site_url('subcount/pdf'), '<i class="fa fa-print"></i>', ['class'=>"btn btn-default btn-flat btn-sm", 'target' => '_blank']); ?>
                    <div class='row'>
                        <form action="<?php echo site_url('subcount/index'); ?>" class="form-inline" method="get">
                            <div class='col-md-8'>
                            </div>

                            <div class='col-md-4'>
                                <div class ='form-group form-group-sm pull-right'>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                                        <span class="input-group-btn">
							  <button class="btn btn-sm btn-flat btn-primary" type="submit"><i class='fa fa-search'></i> Cari</button>
											<?php
											if ($q <> '')
											{
												?>
                                                <a href="<?php echo site_url('subcount'); ?>" class="btn btn-sm btn-flat btn-default"><i class='fa fa-minus-square-o'></i>&nbsp;Reset</a>
												<?php
											}
											?>
							</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br>
                    <br>
                    <div class='table-responsive'>
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <tr>
                                <th>No</th>
                                <th>Nama Subcount</th>
                                <th>Alamat</th>
                                <th>No Telp</th>
                                <th>Action</th>
                            </tr><?php
							foreach ($subcount_data as $subcount)
							{
								?>
                                <tr>
                                    <td width="80px"><?php echo ++$start ?></td>
                                    <td><?php echo $subcount->nama_subcount ?></td>
                                    <td><?php echo $subcount->alamat ?></td>
                                    <td><?php echo $subcount->no_telp ?></td>
                                    <td style="text-align:center" width="140px">
										<?php
										echo anchor(site_url('subcount/read/'.$subcount->id_subcount),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-info btn-xs'));
										echo '  ';
										echo anchor(site_url('subcount/update/'.$subcount->id_subcount),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-primary btn-xs'));
//										echo '  ';
//										echo anchor(site_url('subcount/delete/'.$subcount->id_subcount),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-xs" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
										?>
                                    </td>
                                </tr>
								<?php
							}
							?>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
                        </div>
                        <div class="col-md-6 text-right">
							<?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

