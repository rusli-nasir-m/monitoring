
<?php
 $options = [
    0 => 'Sample1',
    1 => 'Sample2',
 ];
?>
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box box-primary'>
                <div class='box-header'>                
                  <h3 class='box-title'>SUBCOUNT</h3>
                  </div>
                  <div class='box-body'>
                      
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Nama Subcount</td>
            <td>
            <input type="text" class="form-control" name="nama_subcount" id="nama_subcount" placeholder="Nama Subcount" value="<?php echo $nama_subcount; ?>" />
            <?php echo form_error('nama_subcount') ?>
        </td>
	    <tr><td>Alamat</td>
            <td>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
            <?php echo form_error('alamat') ?>
        </td></tr>
	    <tr><td>No Telp</td>
            <td>
            <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="No Telp" value="<?php echo $no_telp; ?>" />
            <?php echo form_error('no_telp') ?>
        </td>
	    <input type="hidden" name="id_subcount" value="<?php echo $id_subcount; ?>" />
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('subcount') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table>
    </form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->