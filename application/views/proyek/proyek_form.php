
<?php
 $options = [
    0 => 'Sample1',
    1 => 'Sample2',
 ];
?>
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box box-primary'>
                <div class='box-header'>                
                  <h3 class='box-title'>PROYEK</h3>
                  </div>
                  <div class='box-body'>
                      
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Nama Proyek</td>
            <td>
            <input type="text" class="form-control" name="nama_proyek" id="nama_proyek" placeholder="Nama Proyek" value="<?php echo $nama_proyek; ?>" />
            <?php echo form_error('nama_proyek') ?>
        </td>
	    <tr><td>Alamat Proyek</td>
            <td>
            <textarea class="form-control" rows="3" name="alamat_proyek" id="alamat_proyek" placeholder="Alamat Proyek"><?php echo $alamat_proyek; ?></textarea>
            <?php echo form_error('alamat_proyek') ?>
        </td></tr>
	    <tr><td>Tahun Proyek</td>
            <td>
            <input type="text" class="form-control" name="tahun_proyek" id="tahun_proyek" placeholder="Tahun Proyek" value="<?php echo $tahun_proyek; ?>" />
            <?php echo form_error('tahun_proyek') ?>
        </td>
	    <input type="hidden" name="id_proyek" value="<?php echo $id_proyek; ?>" />
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('proyek') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table>
    </form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->