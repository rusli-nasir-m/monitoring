<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function upload_image($fileinput,$path, $file_name = '', $data = 'products'){
    $ci = &get_instance();
	$ci->load->helper('string');
//	$config_image['file_name'] = $fileinput;
    $config_image['upload_path']          = $path . '/' . $data.'/original';
    $config_image['allowed_types']        = 'gif|jpg|png';
//    $config_image['max_size']             = 100;
    $config_image['max_width']            = 8000;
    $config_image['max_height']           = 8000;
    if($file_name != '') {
	    $config_image['file_name'] = $file_name;
    }

    if(!is_dir($config_image['upload_path']))
    {
        mkdir($config_image['upload_path'], 0755, TRUE);
    }

	$ci->load->library('upload', $config_image);
	$image_data = null;
    if ( ! $ci->upload->do_upload($fileinput))
    {
        $error = array('error' => $ci->upload->display_errors());
//        show_error($ci->upload->display_errors(),500,'Error Upload');
        $data = [
           'status' =>false,
            'error' =>$error
        ];
    }
    else
    {
        $image_data = $ci->upload->data();
		$time = $file_name;
        $source_img = $image_data['full_path']; //Defining the Source Image
	    if($file_name){
		    _mkdir($path . '/'.$data.'/100x100');
		    $new_img_100 = $path . '/'.$data.'/100x100/' .$file_name.$image_data['file_ext']; //Defining the Destination/New Image
		    _mkdir($path . '/'.$data.'/250x250');
		    $new_img_250 = $path . '/'.$data.'/250x250/' .$file_name.$image_data['file_ext']; //Defining the Destination/New Image
//		    $new_img = $image_data['file_path'] .'_thumb_'.$file_name.$image_data['file_ext']; //Defining the Destination/New Image
	    }else{
		    _mkdir($path . '/'.$data.'/100x100');
		    $new_img_100 = $path . '/'.$data.'/100x100/' .$image_data['raw_name'].$image_data['file_ext']; //Defining the Destination/New Image
		    _mkdir($path . '/'.$data.'/250x250');
		    $new_img_250 = $path . '/'.$data.'/250x250/' .$image_data['raw_name'].$image_data['file_ext']; //Defining the Destination/New Image
	    }
        create_thumb_gallery($file_name,$image_data, $source_img, $new_img_100, 100, 100);
        create_thumb_gallery($file_name,$image_data, $source_img, $new_img_250, 250, 250);
        $data = [
            'status' =>true,
            'image_data' =>$image_data,
			'image_name' => $time.$image_data['file_ext']
        ];
        $data['source_image_250'] = $new_img_250;
        $data['source_image_100'] = $new_img_100;

    }

    return $data;
}

function create_thumb_gallery($file_name,$upload_data, $source_img, $new_img, $width, $height)
{
    $ci = &get_instance();
//Copy Image Configuration
    $config['image_library'] = 'gd2';
    $config['source_image'] = $source_img;
    $config['create_thumb'] = FALSE;
    $config['new_image'] = $new_img;
    $config['quality'] = '100%';

    $ci->load->library('image_lib');
    $ci->image_lib->initialize($config);

    if ( ! $ci->image_lib->resize() )
    {
	    show_error($ci->image_lib->display_errors(),500,'Error Create Thumbnail');
    }
    else
    {
//Images Copied
//Image Resizing Starts
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source_img;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['quality'] = '100%';
        $config['new_image'] = $source_img;
        $config['overwrite'] = TRUE;
        $config['width'] = $width;
        $config['height'] = $height;
        $dim = (intval($upload_data['image_width']) / intval($upload_data['image_height'])) - ($config['width'] / $config['height']);
        $config['master_dim'] = ($dim > 0)? 'height' : 'width';

        $ci->image_lib->clear();
        $ci->image_lib->initialize($config);

        if ( ! $ci->image_lib->resize())
        {
	        show_error($ci->image_lib->display_errors(),500,'Error Create Thumbnail');

        }
        else
        {
	        $ci->image_lib->clear();
//echo 'Thumnail Created';
            return true;
        }
    }
}