<?php
function cmb_dinamis($name,$table,$field,$pk,$selected){
    $ci = get_instance();
    $cmb = "<select name='$name' class='form-control'>";
    $data = $ci->db->get($table)->result();
    foreach ($data as $d){
        $cmb .="<option value='".$d->$pk."'";
        $cmb .= $selected==$d->$pk?" selected='selected'":'';
        $cmb .=">".  strtoupper($d->$field)."</option>";
    }
    $cmb .="</select>";
    return $cmb;  
}

function get_image_staff($image,$thumbSize = null){
	if($thumbSize !=null){
		$dataImage = base_url("assets/media/image/staff/{$thumbSize}x{$thumbSize}/" . $image);
		if(getimagesize($dataImage) != false){

			return $dataImage;
		}else{
			return base_url('assets/images/noimage.jpg');
		}
	}else{
		$dataImage = base_url('assets/media/image/staff/original/' . $image);
		if(getimagesize($dataImage) != false){
			return $dataImage;
		}else{
			return base_url('assets/images/noimage.jpg');
		}
	}
	print_r($dataImage);


}


function get_proyek($id){
	$ci = get_instance();
	if($id){
		$ci->db->where('id_proyek',$id);
		$query = $ci->db->get('proyek');
		if($query){
			return $query->row()->nama_proyek;
		}
	}
	return "Tidak terdaftar";
}


function get_gudang($id){
	$ci = get_instance();
	if($id){
		$ci->db->where('id_gudang',$id);
		$query = $ci->db->get('gudang');
		if($query){
			return $query->row()->nama_gudang;
		}
	}
	return "Tidak terdaftar";
}

function get_subcount($id){
	$ci = get_instance();
	if($id){
		$ci->db->where('id_subcount',$id);
		$query = $ci->db->get('subcount');
		if($query){
			return $query->row()->nama_subcount;
		}
	}
	return "Tidak terdaftar";
}

//Make directory for image uploading
function _mkdir($target)
{
	// from php.net/mkdir user contributed notes
	if(file_exists($target))
	{
		if( ! @is_dir($target))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	// Attempting to create the directory may clutter up our display.
	if(@mkdir($target))
	{
		$stat = @stat(dirname($target));
		$dir_perms = $stat['mode'] & 0007777;  // Get the permission bits.
		@chmod($target, $dir_perms);
		return true;
	}
	else
	{
		if(is_dir(dirname($target)))
		{
			return false;
		}
	}

	// If the above failed, attempt to create the parent node, then try again.
	if ($this->_mkdir(dirname($target)))
	{
		return $this->_mkdir($target);
	}

	return false;
}

function dmyToYmd($date_string){
	$a = explode('-',$date_string);
	$my_new_date = $a[2].'-'.$a[1].'-'.$a[0];

	return $my_new_date;
}

function ymdToDmy($date_string){
	$newDate = date("d-m-Y", strtotime($date_string));
	return $newDate; // => 2013-12-24
}

function get_subcount_name($id_Subcount){
	$ci = &get_instance();
	$ci->load->model('Subcount_model');
	$subcount = $ci->Subcount_model->get_by_id($id_Subcount);
	if($subcount){
		return $subcount->nama_subcount;
	}else{
		return '-';
	}
}

function get_proyek_name($id_Subcount){
	$ci = &get_instance();
	$ci->load->model('Proyek_model');
	$subcount = $ci->Proyek_model->get_by_id($id_Subcount);
	if($subcount){
		return $subcount->nama_proyek;
	}else{
		return '-';
	}
}