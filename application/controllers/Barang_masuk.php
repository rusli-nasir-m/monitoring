<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Barang_masuk extends CI_Controller
{


	function __construct()
	{
		parent::__construct();
		$this->load->model('Barang_masuk_model');
		$this->load->model('Detail_masuk_model');
		$this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
		} elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
            <script>
                alert('Silahkan Login Terlebih Dahulu!');
                window.location.href = "<?=site_url('auth/login')?>";
            </script>
			<?php
		}
	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));
		$dari = intval($this->input->get('dari'));
		$sampai = intval($this->input->get('sampai'));

		$data_array = [
			'dari' => $dari,
			'sampai' => $sampai,
			'q' => $q,
			'start' => $start,
		];

		$link = http_build_query($data_array);
		$config['base_url'] = site_url('barang_masuk/?' . $link);
		$config['first_url'] = site_url('barang_masuk/?' . $link);

		if ($q == '' && $dari && $sampai) {
		    $config['base_url'] = base_url() . 'barang_masuk/';
			$config['first_url'] = base_url() . 'barang_masuk/';
		}
		$data_array = [
			'dari' => ($dari)?dmyToYmd($dari):null,
			'sampai' => ($sampai)?dmyToYmd($sampai):null,
			'q' => $q,
			'start' => $start,
		];
		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Barang_masuk_model->total_rows($data_array);
		$barang_masuk = $this->Barang_masuk_model->get_limit_data($config['per_page'], $start, $data_array);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'barang_masuk_data' => $barang_masuk,
			'c_header' => 'Barang_masuk',
			'c_sub_header' => 'Daftar Barang_masuk',
			'q' => $q,
			'dari' => $dari,
			'sampai' => $sampai,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
		);
		$this->template->load('template','barang_masuk/barang_masuk_list', $data);
	}

	public function read($id)
	{
		$row = $this->Barang_masuk_model->get_by_id($id);
		if ($row) {
			$data = array(
				'c_header' => 'Barang Masuk',
				'c_sub_header' => 'Detail Barang Masuk',
				'id_masuk' => $row->id_masuk,
				'no_po' => $row->no_po,
				'id_subcount' => $row->id_subcount,
				'tanggal' => $row->tanggal,
			);
			$this->template->load('template','barang_masuk/barang_masuk_read', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang_masuk'));
		}
	}

	public function create()
	{
		$data = array(
			'c_header' => 'Barang masuk',
			'c_sub_header' => 'Tambah Barang masuk',
			'button' => 'Create',
			'action' => site_url('barang_masuk/create_action'),
			'is_newRecord' => true,
			'id_masuk' => set_value('id_masuk'),
			'no_po' => set_value('no_po'),
			'id_subcount' => set_value('id_subcount'),
			'tanggal' => set_value('tanggal'),
			'detail_bm' => null,
		);
		$this->template->load('template','barang_masuk/barang_masuk_form', $data);
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$id_user = $this->session->userdata('nip');
			$data = array(
				'no_po' => $this->input->post('no_po',TRUE),
				'id_subcount' => $this->input->post('id_subcount',TRUE),
				'tanggal' => dmyToYmd($this->input->post('tanggal',TRUE)),
				'nip' => $id_user
			);

			$id_bm = $this->Barang_masuk_model->insert($data);
			extract($_POST);
			$i = 0;
			foreach ($id_barang as $key => $value){
				$detailPP = [
					'id_masuk' => $id_bm,
					'id_barang' => $value,
					'jumlah' => $jumlah[$i],
//	                'diterima' => $diterima[$i],
					'satuan' => $satuan[$i],
//	                'diterima_satuan' => $diterima_satuan[$i],
				];
				$this->Detail_masuk_model->insert($detailPP);
				$i++;
			}

			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url("barang_masuk/read/$id_bm"));
		}
	}

	public function update($id)
	{
		$row = $this->Barang_masuk_model->get_by_id($id);
		if ($row) {
			$detailBM = $this->Detail_masuk_model->get_by_id_bm($id);
			$data = array(
				'c_header' => 'Barang masuk',
				'c_sub_header' => 'Ubah Barang masuk',
				'button' => 'Update',
				'action' => site_url('barang_masuk/update_action'),
				'is_newRecord' => false,
				'id_masuk' => set_value('id_masuk', $row->id_masuk),
				'no_po' => set_value('no_po', $row->no_po),
				'id_subcount' => set_value('id_subcount', $row->id_subcount),
				'tanggal' => set_value('tanggal', ymdToDmy($row->tanggal)),
				'detail_bm' => $detailBM,
			);
			$this->template->load('template','barang_masuk/barang_masuk_form', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang_masuk'));
		}
	}

	public function update_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id_masuk', TRUE));
		} else {
			$id_masuk = $this->input->post('id_masuk', TRUE);
			$data = array(
				'no_po' => $this->input->post('no_po',TRUE),
				'id_subcount' => $this->input->post('id_subcount',TRUE),
				'tanggal' => dmyToYmd($this->input->post('tanggal',TRUE)),
			);

			$this->Barang_masuk_model->update($this->input->post('id_masuk', TRUE), $data);
			extract($_POST);
			$i = 0;
			$this->Detail_masuk_model->delete_bybm($id_masuk);

			foreach ($id_barang as $key => $value){
				$detailPP = [
					'id_masuk' => $id_masuk,
					'id_barang' => $value,
					'jumlah' => ($jumlah[$i])? $jumlah[$i]:0,
					'satuan' => ($satuan[$i])? $satuan[$i]:0,
//					'diterima' => ($diterima[$i])? $diterima[$i]:null,
//					'permintaan_satuan' => ($satuan[$i])? $satuan[$i]:null,
//					'diterima_satuan' => ($diterima_satuan[$i])? $diterima_satuan[$i]:null,
				];

				$i++;
				$this->Detail_masuk_model->insert($detailPP);
			}

			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('barang_masuk'));
		}
	}

	public function delete($id)
	{
		$row = $this->Barang_masuk_model->get_by_id($id);

		if ($row) {
			$this->Barang_masuk_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('barang_masuk'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang_masuk'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('no_po', 'no po', 'trim|required');
		$this->form_validation->set_rules('id_subcount', 'id subcount', 'trim|required');
		$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
//	$this->form_validation->set_rules('id_barang[]',"Barang Masuk", "required|trim|xss_clean");
//	$this->form_validation->set_rules('id_barang[]', 'Barang Masuk', 'barang_masuk_check');
		$this->form_validation->set_rules('id_masuk', 'id_masuk', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}


	function barang_masuk_check($something) {
		// Do what you need to validate here
		return true; // or false, depending if your validation succeeded or not

	}

	function pdf()
	{
		$data = array(
			'barang_masuk_data' => $this->Barang_masuk_model->get_all(),
			'start' => 0
		);

		ini_set('memory_limit', '32M');
		$this->load->view('barang_masuk/barang_masuk_pdf', $data);
//		$html = $this->load->view('barang_masuk/barang_masuk_pdf', $data, true);
//		$this->load->library('pdf');
//		$pdf = $this->pdf->load();
//		$pdf->WriteHTML($html);
//		$pdf->Output('barang_masuk.pdf', 'D');
	}

	public function add_row_bm() {
		?>
        <tr>
            <td>
				<?php
				$list_id_barang = $this->db->query("SELECT * FROM barang")->result();
				$list_barang[''] = '-- Pilih Barang --';
				if($list_id_barang){
					foreach ($list_id_barang as $dia){
						$list_barang[$dia->id_barang] = $dia->nama_barang;
					}
				}
				?>
				<?= form_dropdown('id_barang[]',$list_barang,null,[
					'class'=> 'form-control select2'
				])?>
            </td>
            <td>
                <input type="text" class="form-control" name="jumlah[]">
            </td>
            <td>
                <input type="hidden"class="form-control" name="id_detail_masuk[]">
                <input type="text"class="form-control" name="satuan[]">
            </td>
            <td>
                <button type="button" class="tambah btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i></button>
                <button type="button" class="hapus btn btn-danger btn-sm btn-flat"><i class="fa fa-minus"></i></button>
            </td>
        </tr>
		<?php
	}

}

/* End of file Barang_masuk.php */
/* Location: ./application/controllers/Barang_masuk.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-11 09:26:30 */
/* Modification By Rusli */
/* http://harviacode.com */