<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subcount extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Subcount_model');
        $this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');            
        } elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
                    <script>
                        alert('Silahkan Login Terlebih Dahulu!');
                        window.location.href = "<?=site_url('auth/login')?>";
                    </script>
            <?php
		}
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'subcount/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'subcount/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'subcount/';
            $config['first_url'] = base_url() . 'subcount/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Subcount_model->total_rows($q);
        $subcount = $this->Subcount_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'subcount_data' => $subcount,
            'c_header' => 'Subcount',
            'c_sub_header' => 'Daftar Subcount',
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','subcount/subcount_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Subcount_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_subcount' => $row->id_subcount,
		'nama_subcount' => $row->nama_subcount,
		'alamat' => $row->alamat,
		'no_telp' => $row->no_telp,
	    );
            $this->template->load('template','subcount/subcount_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('subcount'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('subcount/create_action'),
            'is_newRecord' => true,
	    'id_subcount' => set_value('id_subcount'),
	    'nama_subcount' => set_value('nama_subcount'),
	    'alamat' => set_value('alamat'),
	    'no_telp' => set_value('no_telp'),
	);
        $this->template->load('template','subcount/subcount_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_subcount' => $this->input->post('nama_subcount',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
	    );

            $this->Subcount_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('subcount'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Subcount_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('subcount/update_action'),
                'is_newRecord' => false,
		'id_subcount' => set_value('id_subcount', $row->id_subcount),
		'nama_subcount' => set_value('nama_subcount', $row->nama_subcount),
		'alamat' => set_value('alamat', $row->alamat),
		'no_telp' => set_value('no_telp', $row->no_telp),
	    );
            $this->template->load('template','subcount/subcount_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('subcount'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_subcount', TRUE));
        } else {
            $data = array(
		'nama_subcount' => $this->input->post('nama_subcount',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
	    );

            $this->Subcount_model->update($this->input->post('id_subcount', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('subcount'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Subcount_model->get_by_id($id);

        if ($row) {
            $this->Subcount_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('subcount'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('subcount'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_subcount', 'nama subcount', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('no_telp', 'no telp', 'trim|required');

	$this->form_validation->set_rules('id_subcount', 'id_subcount', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function pdf()
    {
        $data = array(
            'subcount_data' => $this->Subcount_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
//        $html = $this->load->view('subcount/subcount_pdf', $data, true);
        $this->load->view('subcount/subcount_pdf', $data);
//        $this->load->library('pdf');
//        $pdf = $this->pdf->load();
//        $pdf->WriteHTML($html);
//        $pdf->Output('subcount.pdf', 'D');
    }

}

/* End of file Subcount.php */
/* Location: ./application/controllers/Subcount.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-02 16:46:00 */
/* Modification By Rusli */
/* http://harviacode.com */