<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Barang extends CI_Controller
{


	function __construct()
	{
		parent::__construct();
		$this->load->model('Barang_model');
		$this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
		} elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
            <script>
                alert('Silahkan Login Terlebih Dahulu!');
                window.location.href = "<?=site_url('auth/login')?>";
            </script>
			<?php
		}
	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'barang/?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'barang/?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'barang/';
			$config['first_url'] = base_url() . 'barang/';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Barang_model->total_rows($q);
		$barang = $this->Barang_model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'barang_data' => $barang,
			'c_header' => 'Barang',
			'c_sub_header' => 'Daftar Barang',
			'q' => $q,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
		);
		$this->template->load('template','barang/barang_list', $data);
	}

	public function read($id)
	{
		$row = $this->Barang_model->get_by_id($id);
		if ($row) {
			$data = array(
				'c_header' => 'Detail Barang',
				'c_sub_header' => $row->nama_barang,
				'id_barang' => $row->id_barang,
				'nama_barang' => $row->nama_barang,
				'satuan' => $row->satuan,
			);
			$this->template->load('template','barang/barang_read', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang'));
		}
	}

	public function create()
	{
		$data = array(
			'c_header' => 'Barang',
			'c_sub_header' => 'Tambah Barang',
			'button' => 'Create',
			'action' => site_url('barang/create_action'),
			'is_newRecord' => true,
			'id_barang' => set_value('id_barang'),
			'nama_barang' => set_value('nama_barang'),
			'satuan' => set_value('satuan'),
		);
		$this->template->load('template','barang/barang_form', $data);
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'nama_barang' => $this->input->post('nama_barang',TRUE),
				'satuan' => $this->input->post('satuan',TRUE),
			);

			$this->Barang_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('barang'));
		}
	}

	public function update($id)
	{
		$row = $this->Barang_model->get_by_id($id);

		if ($row) {
			$data = array(
				'c_header' => 'Ubah Barang',
				'c_sub_header' => "$row->nama_barang",
				'button' => 'Update',
				'action' => site_url('barang/update_action'),
				'is_newRecord' => false,
				'id_barang' => set_value('id_barang', $row->id_barang),
				'nama_barang' => set_value('nama_barang', $row->nama_barang),
				'satuan' => set_value('satuan', $row->satuan),
			);
			$this->template->load('template','barang/barang_form', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang'));
		}
	}

	public function update_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id_barang', TRUE));
		} else {
			$data = array(
				'nama_barang' => $this->input->post('nama_barang',TRUE),
				'satuan' => $this->input->post('satuan',TRUE),
			);

			$this->Barang_model->update($this->input->post('id_barang', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('barang'));
		}
	}

	public function delete($id)
	{
		$row = $this->Barang_model->get_by_id($id);

		if ($row) {
			$this->Barang_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('barang'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('nama_barang', 'nama barang', 'trim|required');
		$this->form_validation->set_rules('satuan', 'satuan', 'trim|required');

		$this->form_validation->set_rules('id_barang', 'id_barang', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	function pdf()
	{
		$data = array(
			'barang_data' => $this->Barang_model->get_all(),
			'start' => 0
		);

		ini_set('memory_limit', '32M');
//		$html = $this->load->view('barang/barang_pdf', $data, true);
		$this->load->view('barang/barang_pdf', $data);
//		$this->load->library('pdf');
//		$pdf = $this->pdf->load();
//		$pdf->WriteHTML($html);
//		$pdf->Output('barang.pdf', 'D');
	}

}

/* End of file Barang.php */
/* Location: ./application/controllers/Barang.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-02 16:46:00 */
/* Modification By Rusli */
/* http://harviacode.com */