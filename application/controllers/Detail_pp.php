<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Detail_pp extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Detail_pp_model');
        $this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');            
        } elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
                    <script>
                        alert('Silahkan Login Terlebih Dahulu!');
                        window.location.href = "<?=site_url('auth/login')?>";
                    </script>
            <?php
		}
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'detail_pp/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'detail_pp/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'detail_pp/';
            $config['first_url'] = base_url() . 'detail_pp/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Detail_pp_model->total_rows($q);
        $detail_pp = $this->Detail_pp_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'detail_pp_data' => $detail_pp,
            'c_header' => 'Detail_pp',
            'c_sub_header' => 'Daftar Detail_pp',
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','detail_pp/detail_pp_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Detail_pp_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_detail_pp' => $row->id_detail_pp,
		'id_pp' => $row->id_pp,
		'id_barang' => $row->id_barang,
		'permintaan' => $row->permintaan,
		'diterima' => $row->diterima,
		'permintaan_satuan' => $row->permintaan_satuan,
		'diterima_satuan' => $row->diterima_satuan,
	    );
            $this->template->load('template','detail_pp/detail_pp_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('detail_pp'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('detail_pp/create_action'),
            'is_newRecord' => true,
	    'id_detail_pp' => set_value('id_detail_pp'),
	    'id_pp' => set_value('id_pp'),
	    'id_barang' => set_value('id_barang'),
	    'permintaan' => set_value('permintaan'),
	    'diterima' => set_value('diterima'),
	    'permintaan_satuan' => set_value('permintaan_satuan'),
	    'diterima_satuan' => set_value('diterima_satuan'),
	);
        $this->template->load('template','detail_pp/detail_pp_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_pp' => $this->input->post('id_pp',TRUE),
		'id_barang' => $this->input->post('id_barang',TRUE),
		'permintaan' => $this->input->post('permintaan',TRUE),
		'diterima' => $this->input->post('diterima',TRUE),
		'permintaan_satuan' => $this->input->post('permintaan_satuan',TRUE),
		'diterima_satuan' => $this->input->post('diterima_satuan',TRUE),
	    );

            $this->Detail_pp_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('detail_pp'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Detail_pp_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('detail_pp/update_action'),
                'is_newRecord' => false,
		'id_detail_pp' => set_value('id_detail_pp', $row->id_detail_pp),
		'id_pp' => set_value('id_pp', $row->id_pp),
		'id_barang' => set_value('id_barang', $row->id_barang),
		'permintaan' => set_value('permintaan', $row->permintaan),
		'diterima' => set_value('diterima', $row->diterima),
		'permintaan_satuan' => set_value('permintaan_satuan', $row->permintaan_satuan),
		'diterima_satuan' => set_value('diterima_satuan', $row->diterima_satuan),
	    );
            $this->template->load('template','detail_pp/detail_pp_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('detail_pp'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_detail_pp', TRUE));
        } else {
            $data = array(
		'id_pp' => $this->input->post('id_pp',TRUE),
		'id_barang' => $this->input->post('id_barang',TRUE),
		'permintaan' => $this->input->post('permintaan',TRUE),
		'diterima' => $this->input->post('diterima',TRUE),
		'permintaan_satuan' => $this->input->post('permintaan_satuan',TRUE),
		'diterima_satuan' => $this->input->post('diterima_satuan',TRUE),
	    );

            $this->Detail_pp_model->update($this->input->post('id_detail_pp', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('detail_pp'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Detail_pp_model->get_by_id($id);

        if ($row) {
            $this->Detail_pp_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('detail_pp'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('detail_pp'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_pp', 'id pp', 'trim|required');
	$this->form_validation->set_rules('id_barang', 'id barang', 'trim|required');
	$this->form_validation->set_rules('permintaan', 'permintaan', 'trim|required|numeric');
	$this->form_validation->set_rules('diterima', 'diterima', 'trim|required');
	$this->form_validation->set_rules('permintaan_satuan', 'permintaan satuan', 'trim|required');
	$this->form_validation->set_rules('diterima_satuan', 'diterima satuan', 'trim|required');

	$this->form_validation->set_rules('id_detail_pp', 'id_detail_pp', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function pdf()
    {
        $data = array(
            'detail_pp_data' => $this->Detail_pp_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view('detail_pp/detail_pp_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('detail_pp.pdf', 'D'); 
    }

}

/* End of file Detail_pp.php */
/* Location: ./application/controllers/Detail_pp.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-02 16:46:00 */
/* Modification By Rusli */
/* http://harviacode.com */