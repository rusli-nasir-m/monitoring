<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pp extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pp_model');
        $this->load->model('detail_pp_model','',true);
        $this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');            
        } elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
                    <script>
                        alert('Silahkan Login Terlebih Dahulu!');
                        window.location.href = "<?=site_url('auth/login')?>";
                    </script>
            <?php
		}
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
	    $dari = intval($this->input->get('dari'));
	    $sampai = intval($this->input->get('sampai'));

	    $data_array = [
		    'dari' => $dari,
		    'sampai' => $sampai,
		    'q' => $q,
		    'start' => $start,
	    ];

	    $link = http_build_query($data_array);
	    $config['base_url'] = site_url('pp/?' . $link);
	    $config['first_url'] = site_url('pp/?' . $link);

	    if ($q == '' && $dari && $sampai) {
            $config['base_url'] = base_url() . "pp/";
            $config['first_url'] = base_url() . "pp/";
        }
		
		//if ($q <> '') {
        //    $config['base_url'] = base_url() . 'pp/?q=' . urlencode($q) . "&dari=$dari&sampai=$sampai";
        //    $config['first_url'] = base_url() . 'pp/?q=' . urlencode($q) . "&dari=$dari&sampai=$sampai";
        //} else {
        //    $config['base_url'] = base_url() . "pp/?dari=$dari&sampai=$sampai";
        //    $config['first_url'] = base_url() . "pp/?dari=$dari&sampai=$sampai";
        //}
	    $data_array = [
		    'dari' => ($dari)?dmyToYmd($dari):null,
		    'sampai' => ($sampai)?dmyToYmd($sampai):null,
		    'q' => $q,
		    'start' => $start,
	    ];
        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pp_model->total_rows($data_array);
        $pp = $this->Pp_model->get_limit_data($config['per_page'], $start, $data_array);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pp_data' => $pp,
            'c_header' => 'Order Barang (PP)',
            'c_sub_header' => 'Daftar Order Barang',
            'q' => $q,
            'dari' => $dari,
            'sampai' => $sampai,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','pp/pp_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Pp_model->get_by_id($id);
        if ($row) {
            $data = array(
	            'c_header' => 'Order Barang (PP)',
	            'c_sub_header' => 'Detail Order Barang',
		'id_pp' => $row->id_pp,
		'no_pp' => $row->no_pp,
		'id_proyek' => get_proyek($row->id_proyek),
		'id_gudang' => get_gudang($row->id_gudang),
		'tanggal_pp' => date('d-m-Y',strtotime($row->tanggal_pp)),
	    );
            $this->template->load('template','pp/pp_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pp'));
        }
    }

    public function create() 
    {
        $data = array(
	        'c_header' => 'Order Barang (PP)',
	        'c_sub_header' => 'Tambah Order Barang',
            'button' => 'Create',
            'action' => site_url('pp/create_action'),
            'is_newRecord' => true,
	    'id_pp' => set_value('id_pp'),
	    'no_pp' => '',
	    'id_proyek' => set_value('id_proyek'),
	    'id_gudang' => set_value('id_gudang',1),
	    'tanggal_pp' => date('d-m-Y'),
	        'detail_pp' => null,
	);
        $this->template->load('template','pp/pp_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
	        $kode_pp = $this->Pp_model->getkodeunik();
            $data = array(
		'no_pp' => $kode_pp,
		'id_proyek' => $this->input->post('id_proyek',TRUE),
		'id_gudang' => $this->input->post('id_gudang',TRUE),
		'tanggal_pp' => dmyToYmd($this->input->post('tanggal_pp',TRUE)),
	    );

            $idpp = $this->Pp_model->insert($data);
            extract($_POST);
	        $i = 0;
            foreach ($id_barang as $key => $value){
                $detailPP = [
	                'id_pp' => $idpp,
	                'id_barang' => $value,
	                'permintaan' => $jumlah[$i],
//	                'diterima' => $diterima[$i],
	                'permintaan_satuan' => $satuan[$i],
//	                'diterima_satuan' => $diterima_satuan[$i],
                ];
	            $this->detail_pp_model->insert($detailPP);
	            $i++;
            }

            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url("pp/read/$idpp"));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pp_model->get_by_id($id);

        if ($row) {
            $detailPP = $this->detail_pp_model->get_by_id_pp($id);
            $data = array(
	            'c_header' => 'Order Barang (PP)',
	            'c_sub_header' => 'Update Order Barang',
	            'button' => 'Update',
	            'action' => site_url('pp/update_action'),
	            'is_newRecord' => false,
	            'id_pp' => set_value('id_pp', $row->id_pp),
	            'no_pp' => set_value('no_pp', $row->no_pp),
	            'id_proyek' => set_value('id_proyek', $row->id_proyek),
	            'id_gudang' => set_value('id_gudang', $row->id_gudang),
	            'tanggal_pp' => set_value('tanggal_pp', ymdToDmy($row->tanggal_pp)),
	            'detail_pp' => $detailPP,
	    );
            $this->template->load('template','pp/pp_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pp'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pp', TRUE));
        } else {
	        $id_pp = $this->input->post('id_pp', TRUE);
            $data = array(
		'no_pp' => $this->input->post('no_pp',TRUE),
		'id_proyek' => $this->input->post('id_proyek',TRUE),
		'id_gudang' => $this->input->post('id_gudang',TRUE),
		'tanggal_pp' => dmyToYmd($this->input->post('tanggal_pp',TRUE)),
	    );

            $this->Pp_model->update($id_pp, $data);
	        extract($_POST);
	        $i = 0;
	        $this->detail_pp_model->delete_bypp($id_pp);
	        foreach ($id_barang as $key => $value){
		        $detailPP = [
			        'id_pp' => $id_pp,
			        'id_barang' => $value,
			        'permintaan' => ($jumlah[$i])? $jumlah[$i]:0,
			        'diterima' => ($diterima[$i])? $diterima[$i]:null,
			        'permintaan_satuan' => ($satuan[$i])? $satuan[$i]:null,
			        'diterima_satuan' => ($diterima_satuan[$i])? $diterima_satuan[$i]:null,
		        ];

		        $i++;
	            $this->detail_pp_model->insert($detailPP);
	        }
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url("pp/read/$id_pp"));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pp_model->get_by_id($id);

        if ($row) {
            $this->Pp_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pp'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pp'));
        }
    }

    public function _rules() 
    {
//	$this->form_validation->set_rules('no_pp', 'no pp', 'trim|required');
	$this->form_validation->set_rules('id_proyek', 'id proyek', 'trim|required');
	$this->form_validation->set_rules('id_gudang', 'id gudang', 'trim|required');
	$this->form_validation->set_rules('tanggal_pp', 'tanggal pp', 'trim|required');

	$this->form_validation->set_rules('id_pp', 'id_pp', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function pdf()
    {
        $data = array(
            'pp_data' => $this->Pp_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view('pp/pp_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('pp.pdf', 'D'); 
    }
	public function add_row_pp() {
		?>
        <tr>
            <td>
				<?php
				$list_id_barang = $this->db->query("SELECT * FROM barang")->result();
				$list_barang[''] = '-- Pilih Barang --';
				if($list_id_barang){
					foreach ($list_id_barang as $dia){
						$list_barang[$dia->id_barang] = $dia->nama_barang;
					}
				}
				?>
				<?= form_dropdown('id_barang[]',$list_barang,null,[
					'class'=> 'form-control select2'
				])?>
            </td>
            <td>
                <input type="text" class="form-control" name="jumlah[]">
            </td>
            <td>
                <input type="hidden"class="form-control" name="id_detail_pp[]">
                <input type="text"class="form-control" name="satuan[]">
                <input type="hidden"class="form-control" name="diterima[]">
                <input type="hidden"class="form-control" name="diterima_satuan[]">
            </td>
            <td>
                <button type="button" class="tambah btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i></button>
                <button type="button" class="hapus btn btn-danger btn-sm btn-flat"><i class="fa fa-minus"></i></button>
            </td>
        </tr>
		<?php
	}
}

/* End of file Pp.php */
/* Location: ./application/controllers/Pp.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-02 16:46:00 */
/* Modification By Rusli */
/* http://harviacode.com */