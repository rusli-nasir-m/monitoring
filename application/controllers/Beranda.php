<?php

/**
 * Created by PhpStorm.
 * Project:   monitoring.
 * File Name: Beranda.php.
 * User:      AcenetDev
 * Date:      7/11/2017
 * Time:      11:56 AM
 */
class Beranda extends CI_Controller {

	public function __construct(  ) {
		parent::__construct();
		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
		} elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
			<script>
                alert('Silahkan Login Terlebih Dahulu!');
                window.location.href = "<?=site_url('auth/login')?>";
			</script>
			<?php
		}
	}

	public function index(  ) {
		$data = [
			'c_header' => 'Beranda',
			'c_sub_header' => 'Daftar menu'
		];
		$this->template->load('template','dashboard', $data);
	}
}